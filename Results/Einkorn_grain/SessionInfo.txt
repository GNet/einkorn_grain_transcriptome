R version 3.6.1 (2019-07-05)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux 9 (stretch)

Matrix products: default
BLAS:   /usr/lib/libblas/libblas.so.3.7.0
LAPACK: /usr/lib/lapack/liblapack.so.3.7.0

locale:
 [1] LC_CTYPE=fr_FR.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=fr_FR.UTF-8        LC_COLLATE=fr_FR.UTF-8    
 [5] LC_MONETARY=fr_FR.UTF-8    LC_MESSAGES=fr_FR.UTF-8   
 [7] LC_PAPER=fr_FR.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=fr_FR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets 
[8] methods   base     

other attached packages:
 [1] data.table_1.12.8           coseq_1.10.0               
 [3] SummarizedExperiment_1.16.1 DelayedArray_0.12.2        
 [5] BiocParallel_1.20.1         matrixStats_0.55.0         
 [7] Biobase_2.46.0              GenomicRanges_1.38.0       
 [9] GenomeInfoDb_1.22.0         IRanges_2.20.2             
[11] S4Vectors_0.24.3            BiocGenerics_0.32.0        
[13] RColorBrewer_1.1-2          dplyr_0.8.4                
[15] plyr_1.8.5                  ggpubr_0.2.4               
[17] magrittr_1.5                reshape2_1.4.3             
[19] gplots_3.0.1.2              FactoMineR_2.1             
[21] ggplot2_3.2.1               edgeR_3.28.0               
[23] limma_3.42.1               

loaded via a namespace (and not attached):
 [1] colorspace_1.4-1       ggsignif_0.6.0         class_7.3-15          
 [4] htmlTable_1.13.3       XVector_0.26.0         base64enc_0.1-3       
 [7] rstudioapi_0.10        farver_2.0.3           ggrepel_0.8.1         
[10] bit64_0.9-7            AnnotationDbi_1.48.0   mvtnorm_1.0-12        
[13] HTSCluster_2.0.8       splines_3.6.1          leaps_3.1             
[16] DESeq_1.38.0           robustbase_0.93-5      geneplotter_1.64.0    
[19] knitr_1.27             Formula_1.2-3          annotate_1.64.0       
[22] cluster_2.1.0          png_0.1-7              compiler_3.6.1        
[25] backports_1.1.5        assertthat_0.2.1       Matrix_1.2-17         
[28] lazyeval_0.2.2         acepack_1.4.1          htmltools_0.4.0       
[31] tools_3.6.1            gtable_0.3.0           glue_1.3.1            
[34] GenomeInfoDbData_1.2.2 Rcpp_1.0.3             HTSFilter_1.26.0      
[37] vctrs_0.2.2            gdata_2.18.0           tensorA_0.36.1        
[40] xfun_0.12              stringr_1.4.0          lifecycle_0.1.0       
[43] gtools_3.8.1           XML_3.99-0.3           DEoptimR_1.0-8        
[46] zlibbioc_1.32.0        MASS_7.3-51.4          scales_1.1.0          
[49] memoise_1.1.0          gridExtra_2.3          rpart_4.1-15          
[52] latticeExtra_0.6-29    capushe_1.1.1          stringi_1.4.5         
[55] RSQLite_2.2.0          genefilter_1.68.0      corrplot_0.84         
[58] plotrix_3.7-7          e1071_1.7-3            checkmate_1.9.4       
[61] caTools_1.18.0         rlang_0.4.4            pkgconfig_2.0.3       
[64] compositions_1.40-3    bitops_1.0-6           lattice_0.20-38       
[67] purrr_0.3.3            labeling_0.3           htmlwidgets_1.5.1     
[70] bit_1.1-15.1           tidyselect_1.0.0       DESeq2_1.26.0         
[73] R6_2.4.1               Hmisc_4.3-0            DBI_1.1.0             
[76] pillar_1.4.3           foreign_0.8-71         withr_2.1.2           
[79] survival_2.44-1.1      scatterplot3d_0.3-41   RCurl_1.98-1.1        
[82] nnet_7.3-12            tibble_2.1.3           bayesm_3.1-4          
[85] crayon_1.3.4           Rmixmod_2.1.2.2        KernSmooth_2.23-15    
[88] ellipse_0.4.1          jpeg_0.1-8.1           locfit_1.5-9.1        
[91] grid_3.6.1             blob_1.2.1             digest_0.6.23         
[94] flashClust_1.01-2      xtable_1.8-4           munsell_0.5.0         
