#### Clusters Enrichment analysis
enrichment_analysis <- function(Reference,Gene_List,Alpha){
    ## success in the urn /  For each annotation term, number of annotated genes in the Reference file
    m=table(Reference[,2])
    ## failures in the urn / For each annotation term, number of not annotated genes in the Reference file
    n=length(unique(Reference[,1]))-m
    ## trial effective / number of genes in the gene list 
    k=length(Gene_List[,1])
    ## trial success /  For each annotation term, number of annotated genes in the gene list file
    trial<-merge(Gene_List,Reference)
    x=table(factor(trial[,2],levels=rownames(m)))
    ## Result files
    res=NaN
    Term=rownames(m)
    m=as.numeric(m)
    n=as.numeric(n)
    x=as.numeric(x)
    res=data.frame(Term,Urn_Success=m,Urn_Failures=n,Trial_Success=x,Trial_effective=k,Urn_percentage_Success=signif(100*m/(m+n),3),Trial_percentage_Success=signif(100*x/k,3), Pvalue_over=phyper(x-1,m,n,k,lower.tail=FALSE),Pvalue_under=phyper(x,m,n,k,lower.tail=TRUE))
    res <-res[which(res$Trial_Success!=0),]
    res_over <- res[which(res$Pvalue_over<Alpha),]
    if(nrow(res_over)!=0){res_over[,10] <- "OVER-enriched"}
    res_under <- res[which(res$Pvalue_under<Alpha),]
    if(nrow(res_under)!=0){res_under[,10] <- "UNDER-enriched"}
    res_over_under <- rbind(res_over,res_under)
    colnames(res_over_under)[10] <- c("Decision")
    Results <- list("All_results" = res, "Over_Under_Results" = res_over_under)
    return(Results)
}

Enrichment <- function(Data_Directory, Results_Directory, Project_Name, Title, Contrast_Name, Reference_FileName, Alpha = 0.01){
  
  ## subdirectories
  if (!I(Project_Name %in% dir(Results_Directory))) dir.create(paste0(Results_Directory,"/",Project_Name), showWarnings=FALSE)
  subdirectory <- "Enrichment"
  if (!I(subdirectory %in% dir(paste0(Results_Directory,"/",Project_Name)))) dir.create(paste0(Results_Directory,"/",Project_Name,"/",subdirectory), showWarnings=FALSE)
  if (!I(Title %in% dir(paste0(Results_Directory,"/",Project_Name,"/",subdirectory)))) dir.create(paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title), showWarnings=FALSE)
  
  ## Load Reference File
  Reference <-  read.csv2(paste0(Data_Directory,"/",Reference_FileName),header=TRUE,sep="\t")
  
    ## Input == Gene_List file
    if(!is.null(Contrast_Name)){
    # Load Gene List file
        Gene_List <- read.csv2(paste0(Results_Directory,"/",Project_Name,"/DiffAnalysis/",Contrast_Name,"/",Project_Name,"_",Contrast_Name,"_Id_DEG.txt"),header=FALSE,sep="\t")
        colnames(Gene_List) <- c("Gene_ID")
        
    # Perform enrichment analysis
        Results <- enrichment_analysis(Reference,Gene_List,Alpha)
    
    # Save all results
        write.table(Results$All_results,paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title,"/",Project_Name,"_",Title,"_All_Results.txt"),row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
    # Save significant results (over and under enriched)
        write.table(Results$Over_Under_Results,paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title,"/",Project_Name,"_",Title,"_Over_Under_Results.txt"),row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
  }
  
  
  ## Input == Clusters file
  if(is.null(Contrast_Name)){
    # Load Cluster file (in Coexpression directory)
      Clusters <- read.csv2(paste0(Results_Directory,"/",Project_Name,"/Coexpression/",Title,"/",Project_Name,"_",Title,"_AllClusters.txt"), header = TRUE, sep="\t")
      Clusters <- Clusters[,c("Gene_ID","Clusters")]
      
    # List for the summary cluster file
    l <- list()
    for (i in 1:max(Clusters$Clusters)){
      Gene_List <-as.data.frame(Clusters[which(Clusters$Clusters == i),1])
      colnames(Gene_List) <- c("Gene_ID")
        
      # Perform enrichment analysis
          Results <- enrichment_analysis(Reference,Gene_List,Alpha)
     
      # List for the summary cluster file
          l[[i]] <- Results$Over_Under_Results
          
      # Save all results
          write.table(Results$All_results,paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title,"/",Project_Name,"_",Title,"_Cluster_",i,"_All_Results.txt"),row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
      # Save significant results (over and under enriched)
          write.table(Results$Over_Under_Results,paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title,"/",Project_Name,"_",Title,"_Cluster_",i,"_Over_Under_Results.txt"),row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
      }
      
      ## Summary results of all clusters

      Resume <- l[[1]]$Term
      Resume <- as.data.frame(Resume)
      colnames(Resume) <- "Term"
      Resume[,2] <- 1
      colnames(Resume)[2] <- "Cluster_1"
      
      for(j in 2:length(l)){
          tmp2 <- as.data.frame(l[[j]]$Term)
          colnames(tmp2) <- "Term"
          tmp2[,2] <- 1
          colnames(tmp2)[2] <- paste0("Cluster_",j)
          Resume <- merge(Resume,tmp2,by="Term",all.x=T,all.y=T)
          Resume[is.na(Resume)] <- 0
      }
      
      Resume=cbind(Resume,Nb_enriched_clusters=rowSums(Resume[-1]))
      
      write.table(Resume,paste0(Results_Directory,"/",Project_Name,"/",subdirectory,"/",Title,"/",Project_Name,"_",Title,"_Summary_Clusters_Enrichment.txt"),row.names=FALSE,col.names=TRUE, sep="\t", quote = FALSE)
  }
}


